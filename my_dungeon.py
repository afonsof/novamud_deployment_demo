from novamud import Dungeon, Room, Thing

class Phone(Thing):
    name = "Phone"
    description = "You will need your phone if you want to leave your apartment. Otherwise how will you meet your friends?"
    

class ApartmentRoom(Room):
    name = 'ApartmentRoom'
    description = ("A nice sea view apartment near Carcavelos beach. Not particularly clean but that "
                   "can be taken care of before your parents come by for "
                   "your Sunday dinner that you normally hold at your house.")
    def init_room(self):
        self.add_thing(Phone())   
    
    def add_player(self, player):
        player.energy_level=0
        super().add_player(player)
    
    def register_commands(self):
        return ['sleep',
                'drink_coffee']
    
    def sleep(self, player):
        player.energy_level+=3
    
    def drink_coffee(self, player):
        player.energy_level+=1
    
    def go_to (self, player, other_room_name):
        if not player.carrying or player.carrying.name != "Phone":
            player.tell("Your energy level seems to be fine! But you ain't going nowhere without your phone! How will you contact your friends?")
        elif player.energy_level <10:
            player.tell("You are not going out with so little energy! You should take a nap or drink a couple coffees!")
        else:
            super().go_to(player, other_room_name)



class TascaRoom(Room):
    name = 'TascaRoom'
    description = ("Your friendly local Tasca. The drinks are cheap and passable "
                   "but certainly nothing special. Food is fantasic.")


    def register_commands(self):
        return [
            'drink_beer',
            'drink_wine',
            'drink_tequila',
            'eat_snacks',
            'eat_burrito',
            'alcohol_test'
                            ]

    def add_player(self, player):
        player.drink_level = 0
        player.tell('Enjoy your time with your friends. But don`t drink too much, otherwise you will get into a fight again and the chance is high that you spend your night in prison')
        super().add_player(player)

    def drink_beer(self, player):
        player.drink_level += 1
        player.tell('Mhmmm, love the cold beer after such an exhausting day at uni!'
                    'Your drinking level is {}!'.format(player.drink_level))
    def drink_beer_describe(self):
            return ("Have some beer! If you have a bit too much, you can always "
                    "eat some food to bring yourself down")   
    
    def drink_wine(self,player):
        player.drink_level += 2
        player.tell ('Yummy')

    def drink_wine_describe(self):
            return ("Have some wine! If you have a bit too much, you can always "
                    "eat some food to bring yourself down")
    
    def drink_tequila(self,player):
        player.drink_level +=5
        player.tell ('Arriiiiba')
    
    def drink_tequila_describe(self):
            return ("Have some tequila to get the party started!")
    
    
    def eat_snacks(self,player):
        player.drink_level -= 1
    
    def eat_snacks_describe(self):
            return ('Have some snacks in case you are getting a bit too tipsy.')
    
    def eat_burrito(self,player):
        player.drink_level -=3
    
    def eat_burrito_describe(self):
            return ('Have a burrito in case you are getting a bit too tipsy.')
    
    
    def alcohol_test(self, player):
        if player.drink_level == 0:
            player.tell('Don`t be boring. Go, get some drinks!')
        
        elif 1 <= player.drink_level <= 4:
            player.tell('You are still in the good zone.' 
                        'But be careful to not drink too much, so you can get home safely')
        elif 4 <= player.drink_level <= 10:
            player.tell ('You`ve been drinking quite a lot.' 
                         'You should stop drinking and eat some food instead.')
        elif player.drink_level >10:
            player.tell ('You cleary drank too much.'
                         'Do not think about drinking more, otherwise you might end up in prison.'
                         'Rather enjoy some food!')

    def go_to(self, player, other_room_name):
        if player.drink_level <=10:
            return False
        else:
            super().go_to(player, other_room_name)


class Fee (Thing):
    name = 'Fee'
    description = "You have to pay me if you want to get out of here!"
    
class Prison (Room):
    name = 'Prison'
    description = ("Welcome to the prison. If you are here you have been a bad boy. We will fiz that!")


    def init_room(self):
        self.add_thing(Fee())  
    
    def add_player(self, player):
        self.player.num_fees_paid = 0
        self.player.money = 500
        player.tell ("You have {} money available!".format(player.money), 
                     "You have paid {} fees.".format(player.num_fees_paid))
        super().add_player(player)

#add Fee to Prison?

        
    def register_commands (self):
        return [
            'pay_fee'
        ]

    

#is functionality from other room already here? 
#how can we import stuff from previous rooms?
    
    def pay_fee (self, player):
        player.money -=500
        self.num_fees_paid += 1
        player.tell ("You have paid a 500€ fee and you are broke. Go home and cry!")
    
    def go_to (self, player, other_room_name):
        if self.num_fees_paid == 1:
            self.prisonroom.connect_room(self.apartmentroom)
            player.tell("You are now free to go but do not return to TascaRoom!")
        
            super().go_to(player, other_room_name)
            
        else:
            player.tell ("To leave you must pay a fee!")
    
class my_dungeon (Dungeon):
    
    name = 'my_dungeon'
    description = ("You are now a young adult. You have a crappy apartment"
                   "and a car with which you can go to meet up with your "
                   "friends at a TascaRoom.")

 
    def init_dungeon (self):
        ar = ApartmentRoom(self)
        tr = TascaRoom(self)
        pr = Prison(self)
        ar.connect_room(tr, two_way = True)
        tr.connect_room(pr, two_way = True)
        pr.connect_room(ar, two_way = False)
        self.start_room = ar



if __name__ == '__main__':
    import sys
    host, port = None, None
    if len(sys.argv) == 2:
        host, port = sys.argv[1].split(':')
    my_dungeon(host, port).start_dungeon()
